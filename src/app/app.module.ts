import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientInMemoryWebApiModule} from 'angular-in-memory-web-api'

import { AppRoutingModule } from './app-routing.module.js';
import { AppComponent } from './app.component.js';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component.js';
import { PokemonModule } from './pokemon/pokemon.module.js';
import { InMemoryDataService } from './in-memory-data.service.js';

@NgModule({
    declarations: [
        AppComponent,
        PageNotFoundComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpClientModule,
        HttpClientInMemoryWebApiModule.forRoot(InMemoryDataService, { dataEncapsulation : false }),
        PokemonModule,
        AppRoutingModule,
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {}