import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-page-not-found',
  template: `
    <div class='center'>
      <img src="http://assets.pokemon.com/assets/cms2/img/pokedex/full/035.png"/>
      <h1>😭 Hey, cette page n'existe pas ! 😭</h1>
      <a (click)="goBack()" class="waves-effect waves-teal btn-flat">
        Retourner à l' accueil
      </a>
    </div>
  `,
  styles: [
    `a:hover { color:grey }`
  ]
})
export class PageNotFoundComponent {

  constructor(private router: Router) { }

  goBack() {
    this.router.navigate(['/pokemons']);
  }
 }
